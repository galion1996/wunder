<?php


namespace App\Register;


use App\Client\SessionManager\SessionManager;
use App\Client\SessionManager\SessionManagerInterface;
use App\Form\CreateUserFlow;
use App\Service\Payment\PaymentService;
use App\Service\Payment\WunderPaymentInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class RegisterDependencyProvider
{
    public const FORM_USER_FLOW = 'FORM_USER_FLOW';
    public const SERVICE_PAYMENT = 'SERVICE_PAYMENT';
    public const SESSION_MANAGER_CLIENT = 'SESSION_MANAGER_CLIENT';
    public const ENTITY_MANAGER = 'ENTITY_MANAGER';

    /**
     * @var array
     */
    private $_CONTAINER;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CreateUserFlow
     */
    private $createUserFlow;

    /**
     * @var WunderPaymentInterface
     */
    private $paymentService;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /** @required */
    public function setSession(SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    /** @required */
    public function setEm(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /** @required */
    public function setFlow(CreateUserFlow  $createUserFlow){
        $this->createUserFlow = $createUserFlow;
    }

    /** @required */
    public function setPaymentService(PaymentService  $paymentService){
        $this->paymentService = $paymentService;
    }

    public function provideDependecies(){
        $container = [
            static::ENTITY_MANAGER => $this->em,
            static::SESSION_MANAGER_CLIENT => $this->sessionManager,
            static::FORM_USER_FLOW =>$this->createUserFlow,
            static::SERVICE_PAYMENT=> $this->paymentService

        ];
        $this->_CONTAINER = $container;
    }


    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getProvidedDependency(string $key){
        if(!isset($this->_CONTAINER[$key])){
            throw new Exception(sprintf(
                "Could not resolve dependency %s, try registering it to dependency provider", $key
            ));
        }
        return $this->_CONTAINER[$key];
    }


}
