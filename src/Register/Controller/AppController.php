<?php

namespace App\Register\Controller;
use App\Client\SessionManager\SessionManagerInterface;
use App\Entity\User;
use App\Form\CreateUserFlow;
use App\Register\RegisterFactory;
use App\Service\Payment\PaymentService;
use App\Service\Payment\WunderPaymentInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{

    /**
     * @var RegisterFactory
     */
    private $factory;

    /** @required */
    public function setFactory(RegisterFactory $factory){
        $this->factory = $factory;
    }

    /**
     * @Route("/", name="home", methods={"GET", "POST"})
     */
    public function home()
    {
        $formData = new User(); // Your form data class. Has to be an object, won't work properly with an array.
        $formStep = $this->factory->getSessionManagerClient()->getSession('formStep');
        if(!empty($formStep)){
            $this->factory->getUserFlow()->setFormStepKey($formStep['formStepKey']);
            $this->factory->getUserFlow()->setInstanceId($formStep['instanceId']);
            $this->factory->getUserFlow()->setFormTransitionKey($formStep['formTransitionKey']);
            $this->factory->getUserFlow()->setFormStepKey($formStep['formStepKey']);
            $formData = $formStep['formData'];
        }
        $flow = $this->factory->getUserFlow();
        $flow->bind($formData);
        // form of the current step
        $form = $flow->createForm();
        if ($flow->isValid($form)) {

            $this->factory->getSessionManagerClient()->setSession('formStep',
            [
                'instanceId' => $flow->getInstanceId(),
                'formTransitionKey' => $flow->getFormTransitionKey(),
                'currentStepNumber' => $flow->getCurrentStep(),
                'formStepKey' => $flow->getFormStepKey(),
                'formData' => $formData
            ]);
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                $formData->setPaymentDataId('DUMMY_PAYMENT');
                $em = $this->getDoctrine()->getManager();
                $em->persist($formData);
                $em->flush();

                $flow->reset(); // remove step data from the session

                $paymentData = $this->factory->getPaymentService()->makePayment($formData->getId(),$formData->getIban(),$formData->getAccountOwner());

                if(isset($paymentData['paymentDataId'])){
                    $formData->setPaymentDataId($paymentData['paymentDataId']);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($formData);
                    $this->factory->getSessionManagerClient()->unsetSession('formStep');
                }else{
                    $em->remove($formData);
                }
                $em->flush();

                return $this->redirect($this->generateUrl('typ')  . "?paymentId=".$formData->getPaymentDataId());
            }
        }
        return $this->render('landing/landing.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }

    /**
     * @Route("/TYP", name="typ", methods={"GET", "POST"})
     */
    public function thankYouPage(Request $request)
    {
        return $this->render('landing/confirmation.html.twig', [
            'paymentId' => $request->get('paymentId', false),
        ]);
    }

}
