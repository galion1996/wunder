<?php


namespace App\Register;


use App\Client\SessionManager\SessionManagerInterface;
use App\Form\CreateUserFlow;
use App\Service\Payment\WunderPaymentInterface;
use Doctrine\ORM\EntityManagerInterface;

class RegisterFactory
{
    /**
     * @var RegisterDependencyProvider
     */
    private $dependencyProvider;

    /** @required */
    public function setDependency(RegisterDependencyProvider  $registerDependencyProvider){
        $this->dependencyProvider = $registerDependencyProvider;
        $this->dependencyProvider->provideDependecies();

    }

    /**
     * @return WunderPaymentInterface
     * @throws \Exception
     */
    public function getPaymentService():WunderPaymentInterface{
        return $this->dependencyProvider->getProvidedDependency(RegisterDependencyProvider::SERVICE_PAYMENT);
    }

    /**
     * @return EntityManagerInterface
     * @throws \Exception
     */
    public function getEntityManager():EntityManagerInterface{
        return $this->dependencyProvider->getProvidedDependency(RegisterDependencyProvider::ENTITY_MANAGER);
    }

    /**
     * @return SessionManagerInterface
     * @throws \Exception
     */
    public function getSessionManagerClient():SessionManagerInterface{
        return $this->dependencyProvider->getProvidedDependency(RegisterDependencyProvider::SESSION_MANAGER_CLIENT);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getUserFlow():CreateUserFlow{
        return $this->dependencyProvider->getProvidedDependency(RegisterDependencyProvider::FORM_USER_FLOW);
    }
}
