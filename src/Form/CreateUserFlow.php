<?php


namespace App\Form;


use App\Form\Steps\PersonalnfoForm;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class CreateUserFlow extends FormFlow {


    protected function loadStepsConfig()
    {
        return [
            [
                'label' => 'personalData',
                'form_type' => PersonalnfoForm::class,
            ],
            [
                'label' => 'addressData',
                'form_type' => PersonalnfoForm::class,
            ],
            [
                'label' => 'paymentData',
                'form_type' => PersonalnfoForm::class,
            ],
            [
                'label' => 'confirmation',
            ],
        ];
            }

}
