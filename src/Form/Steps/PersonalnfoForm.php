<?php


namespace App\Form\Steps;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonalnfoForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        switch ($options['flow_step']) {
            case 1:
                $builder->add('first_name', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your name',
                    )
                ]);
                $builder->add('last_name', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your surname',
                    )
                ]);
                $builder->add('phone', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your telephone number',
                    )
                ]);
                break;
            case 2:
                $builder->add('street', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your street',
                    )
                ]);
                $builder->add('house_number', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your house number',
                    )
                ]);
                $builder->add('zip_code', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your zipcode',
                    )
                ]);
                $builder->add('city', TextType::class, [
                    'attr' => array(
                    'placeholder' => 'enter your city',
                    )
                ]);
                break;
            case 3:
                $builder->add('account_owner', TextType::class, [
                    'attr' => array(
                        'placeholder' => 'Enter the owner of the account',
                    )
                ]);
                $builder->add('iban', TextType::class, [
                    'attr' => array(
                        'placeholder' => 'enter your iban number',
                    )
                ]);
        }
    }

    public function getBlockPrefix() {
        return 'personalnfo';
    }

}
