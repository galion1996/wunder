<?php


namespace App\Service\Payment;


interface WunderPaymentInterface
{
    /**
     * @param int $customerId
     * @param string $iban
     * @param string $accountOwner
     * @return array
     */
public function makePayment(int $customerId, string $iban,string $accountOwner):array;
}
