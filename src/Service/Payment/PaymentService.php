<?php


namespace App\Service\Payment;


use GuzzleHttp\Client;

class PaymentService implements WunderPaymentInterface
{
    const PAYMENT_ROUTE = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    /**
     * @var Client
     */
   private  $guzzleClient;

    /**
     * @required
     */
    public function __consturct(){
        $this->guzzleClient = new Client();
    }
    public function makePayment(int $customerId, string $iban, string $accountOwner): array
    {
        $paymentData = json_decode($this->guzzleClient->post(self::PAYMENT_ROUTE, [
            'json' => [
                'customerId' => $customerId,
                'iban' => $iban,
                'owner' => $accountOwner
            ]
        ])->getBody()->getContents(), true, 512);

        return $paymentData;
    }
}
