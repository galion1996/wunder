<?php


namespace App\Client\SessionManager;


interface SessionManagerInterface
{
    /**
     * @param string $key
     * @param array $data
     * @return void
     */
    public function setSession(string $key, array $data):void;

    /**
     * @param string $key
     * @return void
     */
    public function unsetSession(string $key):void;

    /**
     * @param string $key
     * @return array
     */
    public function getSession(string $key): array;
}
