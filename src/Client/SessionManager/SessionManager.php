<?php


namespace App\Client\SessionManager;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionManager implements SessionManagerInterface
{
    /**
     * @var SessionInterface
     */
    private $sessionManager;

    /** @required */
    public function setSessionManager(SessionInterface  $session){
        $this->sessionManager = $session;
    }
    public function setSession(string $key, array $data): void
    {
      $this->sessionManager->set($key,$data);
    }

    public function unsetSession(string $key): void
    {
        $this->sessionManager->remove($key);
    }

    /**
     * @param string $key
     * @return array
     */
    public function getSession(string $key): array
    {
        return $this->sessionManager->get($key) ?? [];
    }
}
