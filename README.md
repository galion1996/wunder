# wunder

## project-setup

rename .env.sample to .env (and adjust by your needs)<br>
docker-compose build<br>
docker-compose up<br>

From php container:
composer install
php bin/console assets:install

1. php bin/console doctrine:database:create
2. php bin/console doctrine:migrations:migrate

## Desciption

To populate the form steps use (/) route <br>

## Comments and thoughts

Becuse I have a full time job at the momment I didn't have time to give this too much time. <br>
I used docker because it is easier to setup project, rather then using XAMP, installing every service and so on. <br>

As far as database structure goes, I went with the easiest approach (that is one table)<br>
We could use separate table for payment data (but that would only make sense if we had a real registration form and user could make multiple payments from that account)<br>

If the payment isn't successfull user is presented with error page and he can try to populate the steps again (the data is deleted from the db, because yet again we don't have real user system so it is hard to track if user has unprocessed data or simillar)
Note: This could be achieved maybe with usage of kafka (assuming the error is due to the payment service failing and not the bad data itself)

The part that needs more improvment is design of the form (but as you mentioned that part is not important for now)

Nice thing to have would be an entrypoint.sh script so the person setting up this project doesn't need to run migrations and ect.
Also a makefile with a simple make start, make stop command instead of using docker-compose

